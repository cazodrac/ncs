package controllers;

import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;
import static org.junit.Assert.assertEquals;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.GET;
import static play.test.Helpers.route;

public class HomeControllerTest extends WithApplication {


    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }


    /** /**
     * Test de la creación de un nuevo artista
     */

    public static void main(String args[]){


        // -----------------------Prueba unitaria 1 - Para Funcionalidad --------------------------


        ArtistaController a = new ArtistaController();

        a.setCrearArtista("Tenemos un nuevo artista");

        System.out.println(a.getCrearArtista());
    }

        // -----------------------Prueba unitaria 2 - Para Funcionalidad --------------------------




























    //--------------------------------------------------------------------------//

    public void testIndex() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/");

        Result result = route(app, request);
        assertEquals(OK, result.status());
    }
}
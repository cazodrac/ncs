package models;

/** Creación clase Usuario.*/

public class Usuario
{

    /** Creación atributos del Usuario*/

    public String tipoUsuario;
    public String password;

    public Usuario(String tipoUsuario, String password)
    {
        this.tipoUsuario = tipoUsuario;
        this.password = password;
    }

    public String getTipoUsuario()
    {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario)
    {
        this.tipoUsuario = tipoUsuario;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}
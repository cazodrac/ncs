package models;

/**Clase cancion
/**Atributos de la cancion
/**Acciones de la cancion*/

public class Cancion
{

    public String nombreCancion;
    public String nombreArtista;
    public String genero;
    public String URL;


    public String getNombreCancion()
    {
        return nombreCancion;
    }

    public void setNombreCancion(String nombreCancion)
    {
        this.nombreCancion = nombreCancion;
    }

    public String getNombreArtista() {return nombreArtista;}

    public void setNombreArtista(String nombreArtista) {this.nombreArtista = nombreArtista;}

    public String getGenero()
    {
        return genero;
    }

    public void setGenero(String genero)
    {
        this.genero = genero;
    }

    public String getURL()
    {
        return URL;
    }

    public void setURL(String URL)
    {
        this.URL = URL;
    }
}
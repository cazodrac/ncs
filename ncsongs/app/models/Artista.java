package models;

/**
 * Creación de clase Artista
 * Atributos de la cuenta Artista
 * Acciones de la cuenta Artista*/

public class Artista

{   public String nombreArtista;
    public String nacionalidad;
    public String listaDeCanciones;
    public String facebook;
    public String descripcion;


    public String getNombreArtista()
       {
        return nombreArtista;
    }

    public String getNacionalidad()
    {
        return nacionalidad;
    }

    public String getListaDeCanciones()
    {
        return listaDeCanciones;
    }

    public String getFacebook()
    {
        return facebook;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setNombreArtista()
    {
        this.nombreArtista =nombreArtista;
    }

    public void setNacionalidad()
    {
        this.nacionalidad = nacionalidad;
    }

    public void setListaDeCanciones(String listaDeCanciones)
    {
        this.listaDeCanciones = listaDeCanciones;
    }

    public void setFacebook(){this.facebook = facebook;}

    public void setDescripcion()
    {
        this.descripcion = descripcion;
    }
}
package dispatchers;

import akka.dispatch.MessageDispatcher;

import play.libs.Akka;

public class AppExecutionContexts
{

    public static MessageDispatcher jdbcDispatchers = Akka.system().dispatchers().lookup("contexts.jdbc-dispatcher");

}
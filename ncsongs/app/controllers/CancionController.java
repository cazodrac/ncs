package controllers;

public class CancionController implements java.io.Serializable
{
    private String agregarCancion;
    private String reproducirCancion;
    private String eliminarCancion;

    public CancionController (){}

    /**
     * Método que se encarga de crear una canción
     **/

    
    public String getAgregarCancion() {
        return agregarCancion;
    }

    public void setAgregarCancion(String agregarCancion) {
        this.agregarCancion = agregarCancion;
    }

    /**
     * Método que se encarga de crear una canción
     **/



    public String getReproducirCancion() {
        return reproducirCancion;
    }

    public void setReproducirCancion(String reproducirCancion) {
        this.reproducirCancion = reproducirCancion;
    }

    /**
     * Método que se encarga de crear una canción
     **/



    public String getEliminarCancion() {
        return eliminarCancion;
    }

    public void setEliminarCancion(String eliminarCancion) {
        this.eliminarCancion = eliminarCancion;
    }
}
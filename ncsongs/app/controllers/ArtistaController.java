package controllers;

/**
 * Creación clase ArtistaController
 * */

public class ArtistaController implements java.io.Serializable
{

    private String crearArtista;
    private String modificarArtista;
    private String eliminarArtista;
    private String verCancionesArtista;


    public ArtistaController(){}

    /**
 * Método que se encarga de crear un artista
 **/

    public String getCrearArtista() {
        return crearArtista;
    }

    public void setCrearArtista(String crearArtista) {
        this.crearArtista = crearArtista;
    }

    /**
 * Método que se encarga de modificar un artista
 **/
    public String getModificarArtista() {
        return modificarArtista;
    }

    public void setModificarArtista(String modificarArtista) {
        this.modificarArtista = modificarArtista;
    }

    /**
 * Método que se encarga de eliminar un artista
 **/

    public String getEliminarArtista() {
        return eliminarArtista;
    }

    public void setEliminarArtista(String eliminarArtista) {
        this.eliminarArtista = eliminarArtista;
    }

    /**
     * Método que se encarga de ver canciones asociadas a un artista.
     **/

    public String getVerCancionesArtista() {
        return verCancionesArtista;
    }

    public void setVerCancionesArtista(String verCancionesArtista) {
        this.verCancionesArtista = verCancionesArtista;
    }
}

// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/lenovo/Documents/NCS/ncsongs/conf/routes
// @DATE:Tue Jul 11 10:23:14 COT 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
